---
layout: default
title: About
---

# Site Documentation

Each Phoenix Protocol Asset is published with a documentation site that is generating using Jekyll.

This documentation is held in each project under the `doc/` directory.  Jekyll will convert Markdown files into HTML and GitLab Pages
will serve this static site.

## Create a _config.yml file

Create a Jekyll configuration file:

**_config.yml**

```yaml
asset_name: The Asset Name
asset_url: The public-facing URL

pages:
  - name: About
    path: README.html
  - name: Installation
    path: INSTALLATION.html
  - ... any other markdown file under doc/
```

## Use this Template

```shell
# to initialize this template
$ git submodule add https://gitlab.com/phoenixprotocolrp/documentation.git doc/_layouts
# push up the changes and submodule

# to update this template
$ git submodule update --remote
# push up the changes
```

**Make sure** to put the Jekyll frontmatter into each Markdown file you would like to render.

```markdown
---
layout: default
title: The title
---
```

# Run Jekyll locally

```shell
$ gem install jekyll
$ jekyll serve -s doc
# open a browser at http://localhost:4000/
```
